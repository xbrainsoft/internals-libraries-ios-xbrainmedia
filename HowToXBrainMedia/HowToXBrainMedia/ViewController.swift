//
//  ViewController.swift
//  HowToXBrainMedia
//
//  Created by Christophe Cadix on 11/01/2016.
//  Copyright © 2016 xbrain. All rights reserved.
//

import UIKit

/**
 This simple application shows how a Media synchronization may be started using the *XBrainMedia* library, along with the
 *XBrainClient* library.
 Things worth knowing:
 - the synchronization may be started by one single method call: `XBMMediaLibrary.enableSynchronization()`.
 - however for *XBrainMedia* to be able to upload the database it needs a URI. The agent is requested to provide it to us
 when it receives a *XBCFileSharingUriRequest* message. *XBrainMedia* automatically does send this message once a connection
 is established with the agent so we have nothing to do here but connect to the agent.
 - once the database is built its upload is started immediately if the URI has been received in the meantime, it waits for it
 ortherwise. Therefore there is no need to synchronize (in the 'thread safe' meaning) the `enableSynchronization` call and
 the URI reception.
 - a delegate (`XBMediaDelegate`, weakly referenced) may be set to be notified when the synchronizaiton starts, completes or
 fails.
 
 When the screen appears:
 - a connection to the agent is initiated
 - the Addressbook synchronization may be started by tapping the provided button
*/
class ViewController: UIViewController, XBMediaDelegate {
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var runningIndicator: UIActivityIndicatorView!
    @IBOutlet weak var logView: UITextView!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSLog("configuration = \(AppConfig.configuration)")
        let versionNumber = XBMMediaLibrary.versionNumber()
        let buildNumber = XBMMediaLibrary.buildNumber()
        
        // set up the UI
        runningIndicator.hidesWhenStopped = true
        logView.text = ""
        startButton.enabled = false
        versionLabel.text = "XBrainMedia-\(versionNumber) (\(buildNumber))"
        
        // set `self` as the synchronization's delegate
        XBMMediaLibrary.setDelegate(self)
        
        // We want to be notified when the connection status changes
        observeConnectionStatusChange()
        
        // Before connecting to the agent we need to configure the connection. The XBrainClient library does the rest.
        initXBrainClient()
        XBCClient.connect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// *XBrainClient* configuration
    private func initXBrainClient() {
        if let agentConfig = AppConfig.agentConfig {
            XBCClient.configure(agentConfig)
        } else {
            NSLog("Was unable to configure XBCClient: no agent config found")
        }
    }
    
    /// Computed property giving the connection state
    private var connectionState: kXBCConnectionState {
        return XBCClient.connectionState()
    }
    
    /// When the connection state changes we log it. Media synchronization is started when connected.
    func onConnectionStateChanged(notification: NSNotification?) {
        switch connectionState {
        case .XBCConnecting: NSLog("[connecting to agent...]")
        case .XBCDisconnected: NSLog("[disconnected from agent]")
        case .XBCConnected:
            NSLog("[connected to agent]")
            if !XBMMediaLibrary.isSynchronizationRunning() {
                startButton.enabled = true
            }
            
        case .XBCConnectedAnonymously: NSLog("[anonymously connected to agent]")
        }
    }
    
    private func observeConnectionStatusChange(enabled: Bool = true) {
        let name = kXBCConnectionStateChanged
        if (enabled) {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onConnectionStateChanged(_:)),
                                                             name: name, object: nil);
        } else {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
        }
    }
    
    @IBAction private func onStartClicked(sender: AnyObject) {
        XBMMediaLibrary.enableSynchronization()
    }
    
    //MARK: XBMediaDelegate
    func onSynchronizationStart(addressbook: XBMMediaLibrary) {
        NSLog("[Delegate] > Synchronization started");
        dispatch_async(dispatch_get_main_queue()) {
            self.runningIndicator.startAnimating()
            self.startButton.enabled = false
            self.logView.text.appendContentsOf("Synchronization started\n")
        }
    }
    
    func onSynchronizationComplete(addressbook: XBMMediaLibrary) {
        NSLog("[Delegate] > Synchronization complete");
        dispatch_async(dispatch_get_main_queue()) {
            self.runningIndicator.stopAnimating()
            self.startButton.enabled = true
            self.logView.text.appendContentsOf("Synchronization complete\n")
        }
    }
    
    func onSynchronizationFailed(addressbook: XBMMediaLibrary) {
        NSLog("[Delegate] > Synchronization failed");
        dispatch_async(dispatch_get_main_queue()) {
            self.runningIndicator.stopAnimating()
            self.startButton.enabled = true
            self.logView.text.appendContentsOf("Synchronization failed\n")
        }
    }
}

