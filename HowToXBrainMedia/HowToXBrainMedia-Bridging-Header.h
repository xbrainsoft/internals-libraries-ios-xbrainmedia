//
//  HowToXBrainMedia-Bridging-Header.h
//  HowToXBrainMedia
//
//  Created by Christophe Cadix on 11/01/2016.
//  Copyright © 2015 xBrainSoft. All rights reserved.
//

#ifndef HowToXBrainMedia_Bridging_Header_h
#define HowToXBrainMedia_Bridging_Header_h

#import "XBrainMedia.h"
#import "XBrainClient.h"

#endif /* HowToXBrainMedia_Bridging_Header_h */
