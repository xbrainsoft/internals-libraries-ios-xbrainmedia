//
//  XBMMediaLibrary.h
//  XBrainMedia
//
//  Created by xBrainsoft on 31/07/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBMProvider.h"

#pragma mark - XBMMediaDelegate

@class XBMMediaLibrary;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - XBMediaDelegate
/**
 * Keeps track of the synchronization's execution status
 */
@protocol XBMediaDelegate <NSObject>
/**
 * Synchronization has started
 *
 * @param sender The <i>XBMMediaLibrary</i> instance
 */
- (void)onSynchronizationStart:(XBMMediaLibrary*)sender;
/**
 * Synchronization has complete
 *
 * @param sender The <i>XBMMediaLibrary</i> instance
 */
- (void)onSynchronizationComplete:(XBMMediaLibrary*)sender;
/**
 * Synchronization has failed
 *
 * @param sender The <i>XBMMediaLibrary</i> instance
 */
- (void)onSynchronizationFailed:(XBMMediaLibrary*)sender;

@end

#pragma mark - XBMMediaLibrary
/**
 *  Main entry point of the framework. Used as a singleton.
 */
@interface XBMMediaLibrary : NSObject

/**
 * Shared instance
 */
+ (XBMMediaLibrary *)sharedInstance;

/**
 * Returns the current version number
 */
+ (NSString*) versionNumber;

/**
 * Returns the current build number
 */
+ (NSString *)buildNumber;

/**
 * Is the synchronization process running ?
 */
+ (BOOL) isSynchronizationRunning;

/**
 * Set the <i>XBMediaDelegate</i> enabling to keep track of the synchronization's execution status
 * @param delegate a <i>XBMediaDelegate</i> 
 */
+ (void) setDelegate:(id<XBMediaDelegate>)delegate;

#pragma mark Providers

/**
 * Get all the providers
 */
+ (NSArray<id<XBMProvider>> *) providers;

/**
 * Add a provider
 * @param provider a <i>XBMProvider</i>
 */
+ (void) addProvider:(id<XBMProvider>)provider;

/**
 * Remove a provider
 * @param provider a <i>XBMProvider</i>
 */
+ (void) removeProvider:(id<XBMProvider>)provider;

#pragma mark Sync
/**
 * Request a synchronization be started. It will start if and only if it is not already runnning.
 */
+ (void) enableSynchronization;

/**
 * Get the last time a synchronization finished
 */
+ (nullable NSDate*) lastSynchronizationDate;

NS_ASSUME_NONNULL_END

@end