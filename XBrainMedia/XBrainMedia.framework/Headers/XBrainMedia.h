//
//  XBrainMedia.h
//  XBrainMedia
//
//  Created by xBrainsoft on 21/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XBrainMedia.
FOUNDATION_EXPORT double XBrainMediaVersionNumber;

//! Project version string for XBrainMedia.
FOUNDATION_EXPORT const unsigned char XBrainMediaVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XBrainMedia/PublicHeader.h>

#import <XBrainMedia/XBMMediaLibrary.h>
#import <XBrainMedia/XBMProvider.h>
