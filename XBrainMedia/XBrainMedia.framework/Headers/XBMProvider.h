//
//  XBMProvider.h
//  XBrainMedia
//
//  Created by xBrainsoft on 19/06/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBMObject.h"

/**
 * Protocol defining a synchronization provider
 */
@protocol XBMProvider <NSObject>

/**
 * Get the provider's name
 */
- (nonnull NSString*) name;

/**
 * Loop through the medias
 *
 * @param forEachBlock  block that should be executed for each element you will provide
 * @param completeBlock block that should be executed once all elements have been provided
 * @param errorBlock    block that should be executed if an error occurs during elements parsing
 */
- (void) forEachMediaObject:(nullable void(^)(XBMObject* _Nonnull item))forEachBlock
                 onComplete:(nullable void(^)())completeBlock
                    onError:(nullable void(^)(NSError* _Nullable error))errorBlock;

@end
