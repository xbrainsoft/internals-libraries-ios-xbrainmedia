//
//  XBMObject.h
//  XBrainMedia
//
//  Created by xBrainsoft on 01/09/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XBMObject : NSObject

@property (nonatomic, assign) unsigned long long trackId;
@property (nonatomic, strong) NSString* trackTitle;
@property (nonatomic, assign) unsigned long long albumId;
@property (nonatomic, strong) NSString* albumTitle;
@property (nonatomic, assign) unsigned long long albumArtistId;
@property (nonatomic, strong) NSString* albumArtist;
@property (nonatomic, assign) unsigned long long trackGenreId;
@property (nonatomic, strong) NSString* trackGenre;

- (id) init;

- (void) reset;

@end
